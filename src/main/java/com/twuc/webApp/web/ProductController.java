package com.twuc.webApp.web;

import com.twuc.webApp.contract.CreateProductRequest;
import com.twuc.webApp.contract.GetProductResponse;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.Optional;

@RestController
public class ProductController {
    @Autowired
    private ProductRepository repository;
    @PostMapping("/api/products")
    public ResponseEntity create(@RequestBody CreateProductRequest createProductRequest) {
        if (createProductRequest.getName() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        Product savedProduct = repository.save(new Product(createProductRequest.getName(), createProductRequest.getPrice(), createProductRequest.getUnit()));
        repository.flush();
        return ResponseEntity.created(URI.create("http://localhost/api/products/"+savedProduct.getId())).build();
    }
    @GetMapping("/api/products/{productId}")
    public ResponseEntity searchProduct(@PathVariable Long productId) {
        Optional<Product> product = repository.findById(productId);
        repository.flush();
        if (!product.isPresent()) {
            return ResponseEntity.status(404).build();
        }
        GetProductResponse productResponse = new GetProductResponse(product.get());
        return ResponseEntity.ok().body(productResponse);
    }
}
